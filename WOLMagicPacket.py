#!/usr/bin/env python
import socket
import struct
import sys


# TODO: ipv6.
class WOLMagicPacket:
    mac_str = ''
    mac_addr = ''
    broadcast_ip = ''
    broadcast_port = 9
    packet = b''

    def __init__(self, mac, ip="", port= 9):
        self.validateSetMAC(mac)
        self.validateSetPort(port)
        if ip== "":
            host_ip = socket.gethostbyname(socket.getfqdn(socket.gethostname()))
            self.broadcast_ip = host_ip[: host_ip.rfind('.') + 1] + '255'
        else:
            self.validateSetIP(ip)
        self.genMagicPacket()


    def __str__(self):
        return 'Broadcast IP:{s.broadcast_ip} Port:{s.broadcast_port} Wake up MAC:{s.mac_str}'.format(s=self)

    # TODO: override.
    # def __repr__(self):
    #     pass
    #
    # def __cmp__(self, other):
    #     pass
    #
    # def __del__(self):
    #     pass

    def validateSetIP(self, ip):
        try:
            nums = ip.split('.')
            if len(nums) != 4 or nums[3] != '255':
                raise Exception
            for n in nums:
                if int(n) < 0 or int(n) > 255:
                    raise Exception
        except:
            raise ValueError("Invalid ip!")
        else:
            self.broadcast_ip = ip

    def validateSetPort(self, port):
        if isinstance(port, int) and 0 <= port <= 65535:
            self.broadcast_port = port
        else:
            raise ValueError("Invalid port!")

    def validateSetMAC(self, mac):
        try:
            if len(mac) != 12:
                mac_str = mac.strip()
                mac_str = mac_str.replace(mac_str[2], '')
            else:
                mac_str = mac
            if len(mac_str) == 12:
                strs = [mac_str[i:i + 2] for i in range(0, 12, 2)]
            else:
                raise Exception
            nums = [int(s, 16) for s in strs]
        except:
            raise ValueError("Invalid MAC!")
        else:
            self.mac_str = '-'.join(strs)
            self.mac_addr = nums

    def genMagicPacket(self):
        data = [255] * 6 + self.mac_addr * 16
        for i in data:
            self.packet += struct.pack('B', i)

    def sendMagicPacket(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        # sock.sendto(self.packet, (self.broadcast_ip, self.broadcast_port))
        sock.connect((self.broadcast_ip, self.broadcast_port))
        sock.send(self.packet)
        sock.close()
        print('Send Wake on LAN Magic Packet to:')
        print(self.__str__())


def usage():
    print(sys.argv[0] + ' MACAddress [BroadcastIP [BroadcastPort]]')


def main():
    try:
        argn = len(sys.argv)
        if argn == 2:
            WOLMagicPacket(sys.argv[1]).sendMagicPacket()
        elif argn == 3:
            WOLMagicPacket(sys.argv[1], sys.argv[2]).sendMagicPacket()
        elif argn == 4:
            WOLMagicPacket(sys.argv[1], sys.argv[2], sys.argv[3]).sendMagicPacket()
        else:
            usage()
    except Exception as e:
        print(e)
        usage()


if __name__ == '__main__':
    main()
